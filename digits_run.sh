#!/bin/bash

nvidia-docker run --name digits -d --rm -p 8888:5000 -p 8877:6006 -v ~/:/host -v ~/SensorVision/digits-data:/data -v ~/SensorVision/digits-jobs:/workspace/jobs --shm-size=12g --ulimit memlock=-1 nvcr.io/nvidia/digits:18.11-caffe
