#!/bin/bash

nvidia-docker run -d --rm -p 8880:80 -p 8881:80 -p 8888:8880 -p 8886:8888 -p 8889:3000 -v ~/SensorVision/dls/data:/data -v ~/SensorVision/dls/database:/home/app/database -v ~/SensorVision/dls/keras:/root/.keras -e DLS_EULA_AGREED=y nvcr.io/partners/deep-learning-studio:cuda9-2.5.0

