#!/bin/bash

nvidia-docker run --name digits5 -d --rm -p 8888:5000 -p 8877:6006 -v ~/:/host -v ~/SensorVision/digits-data:/data -v ~/SensorVision/digits-jobs:/jobs --shm-size=12g --ulimit memlock=-1 nvidia/digits:5.0
