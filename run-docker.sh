#!/bin/bash
# Script for extracting image files from video files

if [ $# -eq 0 ]
then 
	echo "Usage: run-docker.sh [pack_name]"
	echo "------------------------------------"
	echo "[pack_name] options"
	echo "waleedka_bash		= waleedka/modern-deep-learning package"
	echo "waleedka_jupy		= waleedka/modern-deep-learning package"
	echo "floydhub_cpu		= floydhub/dl-docker:cpu"
	echo "floydhub_gpu		= floydhub/dl-docker:gpu"
	echo "deepo_bash		= ufoym/deepo:bash all-in-one"
	echo "deepo_bash27		= ufoym/deepo:py27 all-in-one"
	echo "deepo_bashcaffe27		= ufoym/deepo:caffe-py27-cu90 all-in-one"
	echo "nvidia_caffe27		= NVidia Cuda9 Ubuntu18 Caffe Python2.7"
	echo "cuda8_caffe27		= NVidia Cuda8 Ubuntu16 Caffe Python2.7"
	echo "jcboyd_caffe27		= NVidia Cuda8 Ubuntu16 Caffe Python2.7"
else
	case "$1" in
		waleedka_bash)
		    docker run -it -p 8888:8888 -p 6006:6006 -v ~/:/host waleedka/modern-deep-learning bash;
		    ;;
		waleedka_jupy)
		    docker run -it -p 8888:8888 -p 6006:6006 -v ~/:/host waleedka/modern-deep-learning jupyter notebook --allow-root /host;
		    ;;	 
		floydhub_cpu)
                    docker run -it -p 8888:8888 -p 6006:6006 -v ~/:/host floydhub/dl-docker:cpu bash;
		    ;;
		floydhub_gpu)
		    nvidia-docker run -it -p 8888:8888 -p 6006:6006 -v ~/:/host floydhub/dl-docker:gpu bash;
		    ;;
		deepo_bash)
		    nvidia-docker run -it -p 8888:8888 -p 6006:6006 -v ~/:/host ufoym/deepo bash;
		    ;;
		deepo_bash27)
		    nvidia-docker run -it -p 8888:8888 -p 6006:6006 -v ~/:/host ufoym/deepo:py27 bash;
		    ;;
		deepo_bashcaffe27)
		    nvidia-docker run -it -p 8888:8888 -p 6006:6006 -v ~/:/host ufoym/deepo:caffe-py27-cu90 bash;
		    ;;
		nvidia_caffe27)
		    nvidia-docker run -it --rm  -p 8888:8888 -p 6006:6006 -v ~/:/host nvcr.io/nvidia/caffe:18.08-py2 bash;
		    ;;
		cuda8_caffe27)
		    nvidia-docker run -it --rm  -p 8888:8888 -p 6006:6006 -v ~/:/host nvidia/cuda:caffe27 bash;
		    ;;
		jcboyd_caffe27)
		    nvidia-docker run -it --rm  -p 8888:8888 -p 6006:6006 -v ~/:/host jcboyd/ubuntu-cuda-caffe:latest bash;
		    ;;
		*)
		    echo "Usage: run-docker.sh [pack_name]"
		    echo "------------------------------------"
		    echo "[pack_name] options"
	  	    echo "waleedka_bash		= waleedka/modern-deep-learning package"
		    echo "waleedka_jupy		= waleedka/modern-deep-learning package"
		    echo "floydhub_cpu		= floydhub/dl-docker:cpu"
		    echo "floydhub_gpu		= floydhub/dl-docker:gpu"
		    echo "deepo_bash		= ufoym/deepo:bash all-in-one"
		    echo "deepo_bash27		= ufoym/deepo:py27 all-in-one"
		    echo "deepo_bashcaffe27		= ufoym/deepo:caffe-py27-cu90 all-in-one"
		    echo "nvidia_caffe27		= NVidia Cuda9 Ubuntu18 Caffe Python2.7"
		    echo "cuda8_caffe27		= NVidia Cuda8 Ubuntu16 Caffe Python2.7"
	            echo "jcboyd_caffe27	= NVidia Cuda8 Ubuntu16 Caffe Python2.7"
		    exit 1
	esac
fi 






